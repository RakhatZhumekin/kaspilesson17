package kz.kaspi.KaspiLesson17.util;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.io.File;

public class HibernateUtil {
    private static final SessionFactory sessionFactory = buildSessionFactory();

    private static SessionFactory buildSessionFactory() {
        try {
            return new Configuration().configure(new File("C:/Users/User/IdeaProjects/KaspiLesson17/hibernate.cfg.xml")).buildSessionFactory();
        }
        catch (HibernateException e) {
            e.printStackTrace();
            System.out.println("Could not create an instance of HibernateUtil");
            return null;
        }
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public static void shutdown() {
        getSessionFactory().close();
    }
}
