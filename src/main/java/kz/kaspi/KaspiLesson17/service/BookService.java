package kz.kaspi.KaspiLesson17.service;

import kz.kaspi.KaspiLesson17.dao.DAO;
import kz.kaspi.KaspiLesson17.dao.BookDAO;
import kz.kaspi.KaspiLesson17.Book;

import java.util.List;

public class BookService {

    private static DAO<Book, String> bookDao;

    public BookService() {
        bookDao = new BookDAO();
    }

    public void persist(Book book) {
        bookDao.openSession();
        bookDao.persist(book);
        bookDao.closeSession();
    }

    public Book findByName(String name) {
        bookDao.openSession();
        Book book = bookDao.findByName(name);
        bookDao.closeSession();
        return book;
    }

    public void delete(String name) {
        bookDao.openSession();
        Book book = bookDao.findByName(name);
        bookDao.delete(book);
        bookDao.closeSession();
    }

    public void delete(Book book) {
        bookDao.openSession();
        bookDao.update(book);
        bookDao.closeSession();
    }

    public List<Book> findAll() {
        bookDao.openSession();
        List<Book> library = bookDao.findAll();
        bookDao.closeSession();
        return library;
    }

    public void deleteAll() {
        bookDao.openSession();
        bookDao.deleteAll();
        bookDao.closeSession();
    }
}
