package kz.kaspi.KaspiLesson17.dao;

import org.hibernate.Session;

import java.util.List;

public interface DAO<T, ID> {
    public Session openSession();

    public void closeSession();

    public void persist(T entity);

    public void update(T entity);

    public T findByName(ID id);

    public void delete(T entity);

    public List<T> findAll();

    public void deleteAll();
}
