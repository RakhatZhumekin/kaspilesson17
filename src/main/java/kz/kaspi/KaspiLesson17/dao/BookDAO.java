package kz.kaspi.KaspiLesson17.dao;

import kz.kaspi.KaspiLesson17.Book;
import kz.kaspi.KaspiLesson17.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class BookDAO implements DAO<Book, String>{
    private Session currentSession;

    private Transaction currentTransaction;

    @Override
    public Session openSession() {
        currentSession = HibernateUtil.getSessionFactory().openSession();
        currentTransaction = currentSession.beginTransaction();
        return currentSession;
    }

    @Override
    public void closeSession() {
        currentTransaction.commit();
        currentSession.close();
    }

    public Session getCurrentSession() {
        return currentSession;
    }

    @Override
    public void persist(Book book) {
        getCurrentSession().save(book);
    }

    @Override
    public void update(Book book) {
        getCurrentSession().update(book);
    }

    @Override
    public Book findByName(String name) {
        return getCurrentSession().get(Book.class, name);
    }

    @Override
    public void delete(Book book) {
        getCurrentSession().delete(book);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Book> findAll() {
        return getCurrentSession().createQuery("select b from Book b").list();
    }

    @Override
    public void deleteAll() {
        findAll().forEach(b -> delete(b));
    }
}
