package kz.kaspi.KaspiLesson17;
import jakarta.servlet.*;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import kz.kaspi.KaspiLesson17.service.BookService;

import java.io.IOException;
import java.util.List;

@WebServlet(name = "book-form", value = "/book-form")
public class BookFormServlet extends HttpServlet {

    BookService bookService = new BookService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        getList(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        String author = req.getParameter("author");
        String publsher = req.getParameter("publisher");
        int releaseYear = Integer.parseInt(req.getParameter("releaseYear"));

        bookService.persist(new Book(name, author, publsher, releaseYear));

        getList(req, resp);
    }

    private void getList(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Book> library = bookService.findAll();

        req.setAttribute("books", library);

        getServletContext().getRequestDispatcher("/Library.jsp").forward(req, resp);
    }
}
