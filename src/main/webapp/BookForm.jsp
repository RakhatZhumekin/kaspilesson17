<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Book Form</title>
</head>
<body>
<form name="Book-Form" method="post" action="http://localhost:11037/Lesson17/book-form">
    <table>
        <tr>
            <td>Book name: </td>
            <td><input type="text" name="name" placeholder="Book name"></td>
        </tr>
        <tr>
            <td>Author: </td>
            <td><input type="text" name="author" placeholder="Author"></td>
        </tr>
        <tr>
            <td>Publisher: </td>
            <td><input type="text" name="publisher" placeholder="Publisher"></td>
        </tr>
        <tr>
            <td>Release year: </td>
            <td><input type="number" name="releaseYear" placeholder="Release year"></td>
        </tr>
    </table>
    <input type="submit" value="Submit">
</form>
</body>
</html>
