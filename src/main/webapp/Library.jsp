<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Library</title>
</head>
<body>
    <table>
        <tr>
            <td>Book Name</td>
            <td>Author</td>
            <td>Publisher</td>
            <td>Release Year</td>
        </tr>
        <c:forEach items="${books}" var="item">
            <tr>
                <td>${item.name}</td>
                <td>${item.author}</td>
                <td>${item.publisher}</td>
                <td>${item.releaseYear}</td>
            </tr>
        </c:forEach>
    </table>
    <a href="BookForm.jsp">Add book</a>
</body>
</html>
